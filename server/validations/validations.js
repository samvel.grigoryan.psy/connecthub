import {body} from "express-validator";

export const loginValidation = [
    body('email','Wrong Email').isEmail(),
    body('password', "Password must contain at least 6 symbols").isLength({min: 5}),
]
export const registerValidation = [
    body('email','Wrong Email').isEmail(),
    body('password', "Password must contain at least 6 symbols").isLength({min: 5}),
    body('fullName', "Name must contain at least 3 symbols").isLength({min: 3}),
    body('avatarUrl',"Unsupported url").optional().isURL(),
]
export const postCreateValidation = [
    body('postTitleText').isLength({ min: 5 }).withMessage('Enter Post Title'),
    body('postDescriptionText').isLength({ min: 5 }).withMessage('Enter Post Text'),
];
