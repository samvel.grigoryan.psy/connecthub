import bcrypt from "bcrypt";
import UserModel from "../models/user.js";
import jwt from "jsonwebtoken";
import {HOST_NAME, JWT_EXPIRATION_TIME, PORT, SECRET} from "../constants/appConstants.js";
import * as ResponseTypes from "../constants/responseValues.js";

export const register = async (req, res) => {
  try {
    const password = req.body.password;
    const salt = await bcrypt.genSalt();
    const hash = await bcrypt.hash(password, salt);
    const defaultAvatar = `${HOST_NAME}${PORT}/src/user_default.png`
    const defaultBannerImage = `${HOST_NAME}${PORT}/src/banner_default.jpg`
    const avatar = req.body.avatar || defaultAvatar;
    let banners = []

    for (let index = 0; index < 5; index++) {
      const banner = {
        image: defaultBannerImage,
        id: index
      }
      banners.push(banner)
    }

    const doc = new UserModel({
      fullName: req.body.fullName,
      email: req.body.email,
      passwordHash: hash,
      avatar: avatar,
      banners: banners,
      statistics: []
    })


    const user = await doc.save();
    const token = jwt.sign(
      {
        _id: user._id
      },
      SECRET,
      {
        expiresIn: JWT_EXPIRATION_TIME
      }
    )
    const {passwordHash, ...userData} = user._doc;

    res.json(
      {
        token,
        ...userData
      }
    )
  } catch (err) {
    console.log(err)
    res.json({
      message: "Could not register"
    })
  }
}
export const login = async (req, res) => {
  try {
    const user = await UserModel.findOne({email: req.body.email})
    if (!user) {
      return res.status(404).json({
        message: ResponseTypes.AUTH_WRONG_EMAIL_OR_PASSWORD_MESSAGE
      })
    }
    const isValidPassword = await bcrypt.compare(req.body.password, user._doc.passwordHash);
    if (!isValidPassword) {
      return res.status(404).json({
        message: ResponseTypes.AUTH_WRONG_EMAIL_OR_PASSWORD_MESSAGE
      })
    }
    const token = jwt.sign(
      {
        _id: user._id
      },
      SECRET,
      {
        expiresIn: JWT_EXPIRATION_TIME
      }
    );
    const {passwordHash, ...userData} = user._doc;

    res.json(
      {
        token,
        ...userData
      }
    )
  } catch (err) {
    console.log(err)
    res.json({
      message: "Could not Login"
    })
  }
}
