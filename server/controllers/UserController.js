import UserModel from "../models/user.js";
import path from 'path'
import fs from 'fs'
import {fileURLToPath} from 'url';
import * as ResponseTypes from "../constants/responseValues.js";
import {OK} from "../constants/responseValues.js";
import findFileInDirectory from "../utils/fileFinder.js";

const uploadDirectory = path.join(path.dirname(fileURLToPath(import.meta.url)), 'uploads');

export const getAllUsers = async (req, res) => {
  try {
    const users = await UserModel.find();


    if (!users || users.length === 0) {
      return res.status(ResponseTypes.NOT_FOUND).json({
        message: ResponseTypes.AUTH_NOT_FOUND_MESSAGE
      });
    }

    const usersData = users.map(user => {

      const foundProfileImage = findFileInDirectory(uploadDirectory, user._id.toString())

      let avatar = user._doc.avatar;

      if (foundProfileImage) {
        avatar = foundProfileImage;
      }

      const {_id, fullName, email, comments, banners} = user._doc;
      return {_id, fullName, avatar, email, comments, banners};
    });

    res.json(usersData);
  } catch (err) {
    console.log(err)
    res.json({
      message: ResponseTypes.DATA_LOAD_UNABLE_MESSAGE
    })
  }
}

export const uploadProfileImage = async (req, res) => {
  try {
    if (!fs.existsSync(uploadDirectory)) {
      fs.mkdirSync(uploadDirectory);
    }

    fs.renameSync(req.file.path, path.join(uploadDirectory, `${req.userId}.jpg`));

    res.status(OK).send(ResponseTypes.PROFILE_IMAGE_UPDATED_MESSAGE);

  } catch (err) {
    console.log(err)
    res.json({
      message: ResponseTypes.DATA_LOAD_UNABLE_MESSAGE
    })
  }
}

export const userProfile = async (req, res) => {
  try {
    const user = await UserModel.findById(req.headers.id);

    if (!user) {
      return res.status(ResponseTypes.NOT_FOUND).json({
        message: ResponseTypes.AUTH_NOT_FOUND_MESSAGE
      })
    }

    const foundProfileImage = findFileInDirectory(uploadDirectory, user._id.toString())

    let avatar = user._doc.avatar;

    if (foundProfileImage) {
      avatar = foundProfileImage;
    }

    const {_id, fullName, posts, email, comments, banners} = user._doc;

    res.json(
      {_id, fullName, avatar, email, posts, comments, banners}
    )
  } catch (err) {
    console.log(err)
    res.json({
      message: ResponseTypes.DATA_LOAD_UNABLE_MESSAGE
    })
  }
}
