import PostModel from '../models/post.js'
import * as ResponseTypes from "../constants/responseValues.js";
import UserModel from "../models/user.js";
import {INTERNAL_SERVER_ERROR} from "../constants/responseValues.js";

export const getFeaturedPosts = async (req, res) => {
    try {
        const posts = await PostModel.find({featured: true, available: true}).exec();
        res.json(posts);
    } catch (err) {
        console.log(err);
        res.status(INTERNAL_SERVER_ERROR).json({
            message: ResponseTypes.POSTS_LOAD_UNABLE_MESSAGE
        })
    }
}

export const getAllPosts = async (req, res) => {
    try {
        const posts = await PostModel.find().populate('user').exec();
        res.json(posts);
    } catch (err) {
        console.log(err);
        res.status(INTERNAL_SERVER_ERROR).json({
            message: ResponseTypes.POSTS_LOAD_UNABLE_MESSAGE
        })
    }
}

export const getOne = async (req, res) => {
    try {
        const paramID = req.params.id;
        const id = paramID.replace(/^:/, '');

        const doc = await PostModel.findOneAndUpdate(
            {_id: id},
            {$inc: {viewsCount: 1}},
            {returnDocument: 'after'}
        ).exec();
        if (!doc) {
            return res.status(ResponseTypes.NOT_FOUND).json({
                message: ResponseTypes.POST_LOAD_UNABLE_MESSAGE
            });
        }

        res.json(doc);

    } catch (err) {
        console.log(err);
        res.status(ResponseTypes.INTERNAL_SERVER_ERROR).json({
            message: ResponseTypes.POST_LOAD_UNABLE_MESSAGE
        });
    }
};

export const deletePost = async (req, res) => {
    try {
        const paramID = req.params.id;
        const id = paramID.replace(/^:/, '');

        const doc = await PostModel.findByIdAndDelete(
            {_id: id},
        ).exec();
        if (!doc) {
            return res.status(ResponseTypes.NOT_FOUND).json({
                message: ResponseTypes.POST_DELETE_UNABLE_MESSAGE
            });
        }

        res.json({
            success: true,
            message: ResponseTypes.POST_DELETE_MESSAGE
        });

    } catch (err) {
        console.log(err);
        res.status(ResponseTypes.INTERNAL_SERVER_ERROR).json({
            message: ResponseTypes.POST_DELETE_UNABLE_MESSAGE
        });
    }
};

export const createPost = async (req, res) => {
    try {

        const doc = new PostModel({
            user: req.body.user,
            postTitleText: req.body.postTitleText,
            postDescriptionText: req.body.postDescriptionText,
            postImage: req.body.postImage,
            postId: Date.now() + req.body.user
        });

        const post = await doc.save();

        const user = await UserModel.findById(req.body.user);

        if (!user) {
            return res.status(ResponseTypes.NOT_FOUND).json({message: ResponseTypes.POST_UPDATE_UNABLE_MESSAGE});
        }

        user.posts.push(post);

        await user.save();

        res.json(post)
    } catch (err) {
        res.status(ResponseTypes.INTERNAL_SERVER_ERROR).json({
            message: ResponseTypes.POST_SAVE_UNABLE_MESSAGE
        })

    }
}
