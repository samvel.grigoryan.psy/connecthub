// 1xx (Informational)
export const CONTINUE = 100;
export const SWITCHING_PROTOCOLS = 101;

// 2xx (Successful)
export const OK = 200;
export const CREATED = 201;
export const ACCEPTED = 202;
export const NO_CONTENT = 204;

// 3xx (Redirection)
export const MOVED_PERMANENTLY = 301;
export const FOUND = 302;
export const NOT_MODIFIED = 304;

// 4xx (Client Error)
export const BAD_REQUEST = 400;
export const UNAUTHORIZED = 401;
export const FORBIDDEN = 403;
export const NOT_FOUND = 404;
export const CONFLICT = 409;
export const TOO_MANY_REQUESTS = 429;

// 5xx (Server Error)
export const INTERNAL_SERVER_ERROR = 500;
export const NOT_IMPLEMENTED = 501;
export const SERVICE_UNAVAILABLE = 503;
export const GATEWAY_TIMEOUT = 504;


export const DATA_LOAD_UNABLE_MESSAGE = "Could not load data";
export const NOT_ALLOWED_MESSAGE = "Not Allowed";
export const POST_LOAD_UNABLE_MESSAGE = "Could not load post";
export const POSTS_LOAD_UNABLE_MESSAGE = "Could not load posts";
export const POST_DELETE_UNABLE_MESSAGE = "Could not delete";
export const POST_SAVE_UNABLE_MESSAGE = "Could not save Post";
export const POST_UPDATE_UNABLE_MESSAGE = "Could not save Post";
export const POST_UPDATE_MESSAGE = "Post Updated";
export const PROFILE_IMAGE_UPDATED_MESSAGE = "Post Updated";
export const POST_DELETE_MESSAGE = "Post Deleted";
export const AUTH_NOT_FOUND_MESSAGE = "Post Not Fount";
export const AUTH_WRONG_EMAIL_OR_PASSWORD_MESSAGE = "Wrong email or password";


