import mongoose from "mongoose";
import {MONGOOSE_MODEL_NAME_POST} from "../constants/appConstants.js";

const PostSchema = new mongoose.Schema(
    {
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User",
            required: true
        },

        postId: {
            type: String,
            required: true
        },

        featured: {
            type: Boolean,
        },

        available: {
            type: Boolean,
        },

        postTitleText: {
            type: String,
            required: true
        },

        postDescriptionText: {
            type: String,
            required: true
        },

        postImage: {
            type: String,
        },

        viewsCount: {
            type: Number,
            default: 0
        },

    },
    {timestamps: true},
)
export default mongoose.model(MONGOOSE_MODEL_NAME_POST, PostSchema)
