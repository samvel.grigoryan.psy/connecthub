import mongoose from "mongoose";
import {MONGOOSE_MODEL_NAME_USER} from "../constants/appConstants.js";

const UserSchema = new mongoose.Schema(
    {
        fullName: {
            type: String,
            required: true
        },
        email: {
            type: String,
            required: true,
            unique: true
        },
        passwordHash: {
            type: String,
            required: true
        },
        avatar: String,
        banners: Array,
        posts: Array,
        statistics:Array

    },
    {timestamps: true},
)
export default mongoose.model(MONGOOSE_MODEL_NAME_USER, UserSchema)
