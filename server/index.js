import express from 'express'
import cors from 'cors'
import helmet from 'helmet'
import mongoose from 'mongoose'
import multer from 'multer'
import path from 'path'
import bodyParser  from 'body-parser'
import { fileURLToPath } from 'url';
import checkAuth from "./utils/checkAuth.js";
import * as ValidationController from "./validations/validations.js";
import * as AuthController from "./controllers/AuthController.js";
import * as UserController from "./controllers/UserController.js";
import * as PostController from "./controllers/PostController.js";
import handleValidationErrors from "./utils/handleValidationErrors.js";
import {MONGOOSE_URI, PORT} from "./constants/appConstants.js";
import {getFeaturedPosts} from "./controllers/PostController.js";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const app = express();
app.use(bodyParser.json({ limit: '10mb' }));

app.use(cors());

//Todo test this code is not tested
app.use(
  helmet.contentSecurityPolicy({
      directives: {
          defaultSrc: ["'self'"],
          connectSrc: ["'self'"],
          baseUri: ["'self'"],
          // scriptSrc: ["'self'", 'https://trusted-scripts.com'],
          // styleSrc: ["'self'", 'https://trusted-styles.com'],
          // imgSrc: ["'self'", 'data:'],
          // fontSrc: ["'self'"],
          // objectSrc: ["'none'"],
          // mediaSrc: ["'self'"],
          // frameSrc: ["'none'"],
          // formAction: ["'self'"],
          // frameAncestors: ["'none'"],
      },
  })
);

app.use('/src/', express.static(path.join(__dirname, 'src/images')));
app.use('/controllers/uploads/', express.static(path.join(__dirname, '/controllers/uploads')));

mongoose.connect(MONGOOSE_URI)
    .then(() => {
        console.log('DB Ok')
    })
    .catch(() => {
        console.log('DB Error')
})

const storage = multer.diskStorage({
    destination: (_, __, cb) => {
        cb(null, 'src/images/user-images')
    }, fileName: (_, file, cb) => {
        cb(null, file.originalname)
    }
});

const upload = multer({storage});

app.use(express.json());


app.get('/', (req, res) => {
    res.send('Hello')
})

app.get('/profile/:id', checkAuth, UserController.userProfile)

app.get('/all-users', checkAuth, UserController.getAllUsers)

app.post('/user-profile-image', checkAuth, upload.single('file'),  UserController.uploadProfileImage)

app.post('/auth/login', ValidationController.loginValidation, handleValidationErrors, AuthController.login)

app.post('/auth/register', ValidationController.registerValidation, handleValidationErrors, AuthController.register)

app.post('/create-post', checkAuth, ValidationController.postCreateValidation, handleValidationErrors, PostController.createPost)

app.get('/posts', PostController.getAllPosts)
app.get('/posts-featured', PostController.getFeaturedPosts)

app.get('/posts/:id', PostController.getOne)

app.delete('/posts/:id', checkAuth, PostController.deletePost)


app.listen(PORT, (err) => {
    if (err) {
        return console.log(err)
    }
    console.log(`server OK Port: ${PORT}`)
})

