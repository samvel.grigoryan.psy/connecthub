import path from 'path'
import fs from 'fs'
import {HOST_NAME, PORT} from "../constants/appConstants.js";

const findFileInDirectory = (directoryPath, fileName) => {
  const files = fs.readdirSync(directoryPath);
  for (const file of files) {
    const filePath = path.join(directoryPath, file);
    const stats = fs.statSync(filePath);
    const fileNameWithoutExtension =  file.replace(/\.[^.]+$/, '');

    if (stats.isFile() && fileName === fileNameWithoutExtension) {
      return `${HOST_NAME}${PORT}/controllers/uploads/${file}`
    }

    if (stats.isDirectory()) {
      const foundFilePath = findFileInDirectory(filePath, fileName);
      if (foundFilePath) {
        return foundFilePath;
      }
    }
  }

  return null;
};

export default  findFileInDirectory;
