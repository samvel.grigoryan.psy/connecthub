import jwt from "jsonwebtoken";
import {SECRET} from "../constants/appConstants.js";
import * as ResponseTypes from "../constants/responseValues.js";

export default (req, res, next) => {
    const token = (req.headers.authorization || '');
    if (token) {
        try {
            const decoded = jwt.verify(token, SECRET);
            req.userId = decoded._id;
            next();
        } catch (err) {
            return res.status(ResponseTypes.FORBIDDEN).json({message:ResponseTypes.NOT_ALLOWED_MESSAGE})
        }
    } else {
        return res.status(ResponseTypes.FORBIDDEN).json({
            message: ResponseTypes.NOT_ALLOWED_MESSAGE
        })
    }
}
