import {body, validationResult} from "express-validator";
import {BAD_REQUEST} from "../constants/responseValues.js";

export default (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(BAD_REQUEST).json(errors.array())
    }
    next();
}
