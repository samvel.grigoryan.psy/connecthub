import React, {SVGProps} from "react";
import {useTranslation} from "react-i18next";
import routes from "@/routes/Routes";
import {Link} from "react-router-dom";
import NavigationItem from "@/components/navigation/navigationItem/NavigationItem";
import logo from "@/asset/resource/images/logo_transparent.png"
import {APP_NAME, LANGUAGES} from "@/constants/AppConstants";
import Dropdown from "@/components/ui/dropdown/Dropdown";
import {setLanguage} from "@/helpers/languageHelper/setLanguage";
import {getStoredLanguage} from "@/helpers/languageHelper/getStoredLanguage";
import nvStyles from "./Navigation.module.scss"


const Navigation = () => {
    const {t} = useTranslation();
    const { langTitle } = getStoredLanguage();

    const selectLanguage = (language: string) => {
        setLanguage(language)
    }

    function getNavItem(path:string, title:string, icon:React.FC<SVGProps<SVGSVGElement>>) {
        return  path !== "/"
            ? <NavigationItem key={path} path={path} title={title} icon={icon}/>
            : <React.Fragment key={path}/>;
    }

    return (
        <>
            <div className={`${nvStyles.nav_spacer}`}></div>
            <nav className={`${nvStyles.nav}`}>
                <div className={`${nvStyles.logo}`}>
                    <Link className={`${nvStyles.logo}`} to={'/'}>
                        <img src={logo} alt={APP_NAME}/>
                    </Link>
                </div>
                <div className={`${nvStyles.items}`}>
                    {routes.map(({path, title, icon}) => (
                       getNavItem(path, title, icon)
                    ))}
                </div>
                <Dropdown options={Object.values(LANGUAGES)}
                            placeholder={langTitle}
                            onChange={selectLanguage}/>
            </nav>
        </>
    )
}

export default Navigation;

