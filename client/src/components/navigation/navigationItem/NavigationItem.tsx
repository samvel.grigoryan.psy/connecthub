import React from "react";
import {Link} from "react-router-dom";
import {useTranslation} from "react-i18next";
import {INavigationItem} from "@/interface/navigationItem/INavigationItem";
import itemStyles from "./NavigationItem.module.scss"

const NavigationItem: React.FC<INavigationItem> = (props) => {
    const {t} = useTranslation();
    const ArrowUpIcon = props.icon;
    return (
        <Link className={`${itemStyles.item}`} key={props.path} to={props.path}>
            <ArrowUpIcon className={`${itemStyles.item_icon}`}/>
            <span className={`${itemStyles.item_text} base-3`}>{t(props.title)}</span>
        </Link>)
}

export default NavigationItem;
