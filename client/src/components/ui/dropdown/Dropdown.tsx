import React, {useEffect, useRef, useState} from "react";
import ddStyles from './Dropdown.module.scss'
import {IDropdown} from "@/interface/iu/IUi";
import ArrowUpIcon from "@/asset/resource/images/icons/arrow-dropdown.svg";

const Dropdown: React.FC<IDropdown> = (props) => {
    const [isOpen, setIsOpen] = useState(false);
    const dropdownRef = useRef<HTMLDivElement|null>(null);

    const toggleOpen = () => {
        setIsOpen(!isOpen)
    }

    const changeSelected = (item:string) => {
        setIsOpen(false);
        props.onChange(item);
    }

    useEffect(() => {
        const handleClickOutside = (event: MouseEvent) => {
            if (dropdownRef.current && !dropdownRef.current!.contains(event.target as Node)) {
                setIsOpen(false);
            }
        };

        document.addEventListener("click", handleClickOutside);

        return () => {
            document.removeEventListener("click", handleClickOutside);
        };
    }, []);


    return (
        <div ref={dropdownRef} className={`${ddStyles.dropdown_wrapper} ${isOpen ? ddStyles.open : ''}`} onClick={toggleOpen}>
            <div className={ddStyles.selected_value}>
                {props.placeholder}
                <ArrowUpIcon className={`${ddStyles.icon}`}/>
            </div>
            <div className={ddStyles.available_list_wrapper}>
                <div className={ddStyles.available_list}>
                    {props.options.map((item, index) => (
                        <div key={index} className={ddStyles.available_item} onClick={() => changeSelected(item)}>
                            {item}
                        </div>
                    ))}
                </div>
            </div>
        </div>

    )
}

export default Dropdown;
