import React from "react";
import {useTranslation} from "react-i18next";
import {IPostList} from "@/interface/post/IJobPost";
import ftStyles from "./Featured.module.scss"
import HotIcon from "@/asset/resource/images/icons/hot.svg";


const Featured: React.FC<IPostList> = ({data}) => {
    const {t} = useTranslation();
    return (
        <>
            {data.map((post) => (
                <div key={post._id}>
                    <HotIcon/>
                    <h4 className={`t4`}>{post.postTitleText}</h4>
                    <h4 className={`t5`}>{post.postDescriptionText}</h4>
                    <img src={post.postImage} alt=""/>
                </div>
            ))}
        </>
    )
}

export default Featured;
