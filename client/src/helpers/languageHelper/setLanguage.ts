import {LANGUAGE_KEY, LANGUAGES} from "@/constants/AppConstants";
import i18n from "i18next";

export const setLanguage = (language: string) => {
    let key = Object.keys(LANGUAGES)[0];
    switch (language) {
        case LANGUAGES.ru:
            key = 'ru'
            break
        case LANGUAGES.am:
            key = 'am'
            break
    }
    localStorage.setItem(LANGUAGE_KEY, key)
    i18n.changeLanguage(key);
}

