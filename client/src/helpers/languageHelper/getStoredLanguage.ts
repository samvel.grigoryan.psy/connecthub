import {LANGUAGE_KEY, LANGUAGES} from "@/constants/AppConstants";
import {LanguageKey, LanguageInfo} from "@/interface/helpers/IHelper"

export const getStoredLanguage = (): LanguageInfo => {
    let storedLang = localStorage.getItem(LANGUAGE_KEY);

    if (storedLang && storedLang.length > 0 && LANGUAGES[storedLang as LanguageKey]) {
        return {
            langKey: storedLang as LanguageKey,
            langTitle: LANGUAGES[storedLang as LanguageKey]
        };
    }

    return {
        langKey: Object.keys(LANGUAGES)[0] as LanguageKey,
        langTitle: LANGUAGES[Object.keys(LANGUAGES)[0] as LanguageKey]
    };
};
