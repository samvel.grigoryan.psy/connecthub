import React from "react";

const HomePage = React.lazy(() => import('../pages/HomePage'));
const JobsPage = React.lazy(() => import('../pages/JobsPage'));
const TrainingsPage = React.lazy(() => import('../pages/TrainingsPage'));
const CompaniesPage = React.lazy(() => import('../pages/CompaniesPage'));

import homeIcon from "@/asset/resource/images/icons/register.svg"
import jobsIcon from "@/asset/resource/images/icons/jobs.svg"
import trainingsIcon from "@/asset/resource/images/icons/training.svg"
import companiesIcon from "@/asset/resource/images/icons/company.svg"

const routes = [
    {
        path: "/home",
        element: HomePage,
        title: "homeTitle",
        icon: homeIcon
    },
    {
        path: "/jobs",
        element: JobsPage,
        title: "jobsTitle",
        icon: jobsIcon
    },
    {
        path: "/trainings",
        element: TrainingsPage,
        title: "trainingsTitle",
        icon: trainingsIcon
    },
    {
        path: "/companies",
        element: CompaniesPage,
        title: "companiesTitle",
        icon: companiesIcon
    },
    {
        path: "/",
        element: HomePage,
        title: "homeTitle",
        icon: homeIcon
    },
];

export default routes;
