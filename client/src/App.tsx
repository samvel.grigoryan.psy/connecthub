import React, {Suspense} from 'react';
import {I18nextProvider} from "react-i18next";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import {Provider} from "react-redux";
import i18n from './i18n';
import routes from "@/routes/Routes";
import {store} from './store'
import {ModalState} from "@/contextApi/ModalContext";
import Navigation from "@/components/navigation/navigation/Navigation";

const App = () => {
    return (
        <Provider store={store}>
            <I18nextProvider i18n={i18n}>
                <BrowserRouter>
                    <ModalState>
                        <Navigation/>
                        <Suspense fallback={<div>Load</div>}>
                            <Routes>
                                {routes.map(({path, element}) => (
                                    <Route key={path} path={path} Component={element}/>
                                ))}
                            </Routes>
                        </Suspense>
                    </ModalState>
                </BrowserRouter>
            </I18nextProvider>
        </Provider>
    );
}

export default App;
