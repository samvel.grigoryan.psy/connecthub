export const APP_NAME = 'Connect Hub'
export const SET_ERROR = 'SET_ERROR'
export const GET_ERROR = 'GET_ERROR'
export const CLEAR_ERROR = 'CLEAR_ERROR'

export const SET_LOADER = 'SET_LOADER'
export const GET_LOADER = 'GET_LOADER'
export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';

export const LANGUAGE_KEY = 'langKey';
export const LANGUAGES = {
    'en': 'English',
    'ru': 'Russian',
    'am': 'Armenian',
} as const;


export const TOKEN_KEY = 'token';
export const USER_ID = 'userId';
export const ERROR_CLEAR_TIMEOUT = 10000;
export const MAX_FILE_SIZE = 5242880; // 5 MB
export const ALLOWED_FILES = ['image/jpeg', 'image/png'];


export const INTERSECTION_OBSERVER_PARAMS = {
    root: null,
    rootMargin: '0px',
    threshold: 0.1,
};
