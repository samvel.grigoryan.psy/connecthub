export const API_BASE_URL = 'http://localhost:3500';
export const AUTH_ME = '/profile/:userId';
export const AUTH_REGISTER = '/auth/register';
export const USER_PROFILE_IMAGE = '/user-profile-image';
export const AUTH_LOGIN = '/auth/login';
export const ALL_USERS = '/all-users';
export const POSTS_LIST = '/posts-list';
export const CREATE_POST = '/create-post';
