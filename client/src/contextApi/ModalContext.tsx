import React, {createContext, useState} from "react";
import {IModal} from "@/interface/modial/IModal";

const ModalContext = createContext<IModal>({
    modal: false,
    open: () => {
    },
    close: () => {
    }
})

const ModalState = ({children}: { children: React.ReactNode }) => {
    const [modal, setModal] = useState(false);

    const open = () => setModal(true);
    const close = () => setModal(false);

    return (
        <ModalContext.Provider value={{modal, open, close}}>
            {children}
        </ModalContext.Provider>
    )
}

export {
    ModalState, ModalContext
}
