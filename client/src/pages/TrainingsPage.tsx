import {useTranslation} from "react-i18next";

const TrainingsPage = () => {
    const {t} = useTranslation();

    return (
        <div className={''}>
            <h1>{t('trainingsTitle')}
            </h1>
        </div>
    )
}

export default TrainingsPage;
