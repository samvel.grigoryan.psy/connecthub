import {useTranslation} from "react-i18next";
import {useEffect} from "react";
import {useAppDispatch, useAppSelector} from "@/store";
import * as featuredSlice from '../store/slices/featuredSlice'
import Featured from "@/components/featured/Featured";

const HomePage = () => {
    const {t} = useTranslation();
    const featured = useAppSelector((state) => state.featured)

    const dispatch = useAppDispatch()

    useEffect(() => {
        dispatch(featuredSlice.getTrendingAsync())
    }, [dispatch])


    return (
        <div className={`main-content`}>
            <h1>{t('homeTitle')}</h1>
            <Featured data={featured.data} />
        </div>
    )
}

export default HomePage;
