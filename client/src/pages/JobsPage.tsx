import {useTranslation} from "react-i18next";

const JobsPage = () => {
    const {t} = useTranslation();

    return (
        <div className={''}>
            <h1>{t('jobsTitle')}
            </h1>
        </div>
    )
}

export default JobsPage;
