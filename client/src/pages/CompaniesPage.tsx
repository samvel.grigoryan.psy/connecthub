import {useTranslation} from "react-i18next";
import React from "react";

const CompaniesPage = () => {
    const {t} = useTranslation();

    return (
        <div className={''}>
            <h1>{t('companiesTitle')}
            </h1>
        </div>
    )
}

export default CompaniesPage;
