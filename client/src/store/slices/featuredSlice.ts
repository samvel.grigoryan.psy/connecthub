import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'

import axios from '@/api/axios-connect-hub'
import { RootState } from '@/store'
import  {IPostList} from "@/interface/post/IJobPost";

const initialState: IPostList = {
  data: [],
};

export const getTrendingAsync = createAsyncThunk<
  any,
  void,
  { state: RootState }
>('featured/getFeatured', async () => {
  const response = await axios.get(
    `/posts-featured/`
  )

  return response.data
})

const featuredSlice = createSlice({
  name: 'featured',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getTrendingAsync.fulfilled, (state, { payload }) => {
      state.data = payload
    })
  },
})

export default featuredSlice.reducer
