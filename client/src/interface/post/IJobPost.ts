export  interface IPost {
    _id: string;
    user: string;
    postId: string;
    postTitleText: string;
    postDescriptionText: string;
    postImage: string;
    viewsCount: number;
    featured: boolean;
    available: boolean;
    createdAt: string;
    updatedAt: string;
    __v: number;
}

export  interface IPostList {
    data: IPost[];
}

