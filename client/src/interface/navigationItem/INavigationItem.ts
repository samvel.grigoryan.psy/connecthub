import React, {SVGProps} from "react";

export interface INavigationItem {
    path: string;
    title: string;
    icon: React.FC<SVGProps<SVGSVGElement>>;
}
