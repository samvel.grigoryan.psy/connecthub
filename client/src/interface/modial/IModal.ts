export interface IModal {
    modal: boolean,
    open: () => void
    close: () => void
}