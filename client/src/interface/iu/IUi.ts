import React, {SVGProps} from "react";

export interface IDropdown {
    options: Array<string>;
    placeholder?: string;
    icon?: React.FC<SVGProps<SVGSVGElement>>;
    onChange: (value: any) => void;
}
