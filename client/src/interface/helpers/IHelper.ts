import {LANGUAGES} from "@/constants/AppConstants";

export type LanguageKey = keyof typeof LANGUAGES;

export interface LanguageInfo {
    langKey: LanguageKey;
    langTitle: string;
}