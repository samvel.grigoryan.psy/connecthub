import * as webpack from "webpack";
import buildWebpack from "./config/build/buildWebpack";
import {BuildPaths, BuildMode} from "./config/build/types/types";
import * as path from "path";


interface EnvVariable {
    mode: BuildMode,
    port: number
}

export default (env: EnvVariable) => {

    const pats: BuildPaths = {
        output: path.resolve(__dirname, 'build'),
        entry: path.resolve(__dirname, 'src', 'index.tsx'),
        html: path.resolve(__dirname, 'public', 'index.html'),
        public: path.resolve(__dirname, 'public'),
        src: path.resolve(__dirname, 'src'),
    }

    const config: webpack.Configuration = buildWebpack({
        port: env.port ?? 3000,
        paths: pats,
        mode: env.mode ?? 'development'
    });

    return config
}
