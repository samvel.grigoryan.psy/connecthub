import {ModuleOptions} from "webpack";
import MiniCssExtractorPlugin from "mini-css-extract-plugin";
import {BuildOptions} from "./types/types";

export default function buildLoaders(options: BuildOptions): ModuleOptions['rules'] {
    const isDev = options.mode === 'development';
    const isProd = options.mode === 'production';

    const assetLoader = {
        test: /\.(png|jpg|jpeg|gif)$/i,
        type: 'asset/resource'
    }

    const svgrLoader = {
        test: /\.svg$/i,
        use: [
            {
                loader: '@svgr/webpack',
                options: {
                    icon: true,
                    svgoConfig: {
                        plugins: [
                            {
                                name: 'convertColors',
                                params: {
                                    currentColor: true,
                                }
                            }
                        ]
                    }
                }
            }
        ],
    }

    const cssLoaderWithModules = {
        loader: 'css-loader',
        options: {
            modules: {localIdentName: isDev ? '[path][name]__[local]' : '[hash:base64:8]'},
        }
    }

    const postCssLoader = {
        test: /\.s[ac]ss$/i,
        use: [
            'postcss-loader',
        ],
    }

    const scssLoader = {
        test: /\.s[ac]ss$/i,
        use: [(isDev ? 'style-loader' : MiniCssExtractorPlugin.loader), cssLoaderWithModules, 'sass-loader'],
    }

    const tsLoader = {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
    }

    return [
        svgrLoader,
        assetLoader,
        scssLoader,
        postCssLoader,
        tsLoader
    ]
}
